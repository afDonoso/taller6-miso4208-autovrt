import React from 'react'
import { useState } from 'react'
import { getFirstClickImage } from '../Networking/network'

const Reporte = () => {
  const [reportes, setReportes] = useState([
    {
      fecha: Date(),
      imagen: 'autoVRT/cypress/screenshots/test.spec.js/first-click.png',
      imagenM: 'ruta_imagenM',
      diferencias: 'ruta_diferencias',
    },
  ])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th scope="col">Fecha</th>
          <th scope="col">Imagen Base</th>
          <th scope="col">Imagen Modificada</th>
          <th scope="col">Diferencias</th>
        </tr>
      </thead>
      <tbody>
        {reportes.map((reporte) => (
          <tr>
            <th>{reporte.fecha}</th>
            <td>{getFirstClickImage()}</td>
            <td>{reporte.imagenM}</td>
            <td>{reporte.diferencias}</td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export default Reporte
