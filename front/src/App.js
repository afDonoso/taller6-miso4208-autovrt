import React from 'react'
import './App.css'

import Reporte from './reports/Reporte'

function App() {
  return (
    <div className="App container">
      <button type="button" className="btn btn-primary btn-block mb-3">
        Generar Reporte
      </button>
      <Reporte />
    </div>
  )
}

export default App
