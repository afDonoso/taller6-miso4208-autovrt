context('Creating a new palette', () => {
  it('User presses on generate palette', () => {
    cy.visit('https://afdonoso.gitlab.io/taller6-miso4208')
    cy.get('[onclick="randomPalette()"]').click()
    cy.screenshot('first-click')

    cy.get('[onclick="randomPalette()"]').click()
    cy.screenshot('second-click')
  })
})
