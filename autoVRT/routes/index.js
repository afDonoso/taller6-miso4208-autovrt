var express = require('express')
var router = express.Router()
var path = require('path')

const cypress = require('cypress')
const resemble = require('node-resemble-js')
const fs = require('fs')

/* GET home page. */
router.get('/', function (req, res, next) {
  cypress
    .run({ spec: './cypress/integration/test.spec.js' })
    .then(() => {
      res.send({ fecha: Date() })
    })
    .catch((error) => {
      console.error(error)
    })
})

router.get('/imagen', function (req, res, next) {
  res.sendFile(path.resolve('cypress/screenshots/test.spec.js/first-click.png'))
})

router.get('/imagen-m', function (req, res, next) {
  res.sendFile(
    path.resolve('cypress/screenshots/test.spec.js/second-click.png')
  )
})

router.get('/resemble', function (req, res, next) {
  var diff = resemble(
    path.resolve('cypress/screenshots/test.spec.js/first-click.png')
  )
    .compareTo(
      path.resolve('cypress/screenshots/test.spec.js/second-click.png')
    )
    .onComplete((data) => {
      data
        .getDiffImage()
        .pack()
        .pipe(fs.createWriteStream(path.resolve('public/images/') + 'diff.png'))
      res.send(data)
    })
})

router.get('/resemble-result', function (req, res, next) {
  res.sendFile(path.resolve('public/imagesdiff.png'))
})

module.exports = router
